package day23_1

import solveInputs

fun main() {
    solveInputs { input ->
        val cups = input.chunked(1).map { it.toInt() }
        val next = (cups + cups.first()).zipWithNext().map { it.first to it.second }.toMap().toMutableMap()
        var currentCup = cups.first()

        repeat(100) {
            val firstPick = next.getValue(currentCup)
            val secondPick = next.getValue(firstPick)
            val thirdPick = next.getValue(secondPick)
            next[currentCup] = next.getValue(thirdPick)

            var destinationCup = currentCup - 1
            while (destinationCup in listOf(firstPick, secondPick, thirdPick) || destinationCup < 1) {
                destinationCup--
                if (destinationCup < 1) destinationCup = 9
            }

            val afterDestination = next.getValue(destinationCup)
            next[destinationCup] = firstPick
            next[thirdPick] = afterDestination

            currentCup = next.getValue(currentCup)
        }

        val answer = mutableListOf<Int>()
        answer += 1
        while (answer.size < next.size) answer += next.getValue(answer.last())
        answer.drop(1).joinToString("")
    }
}
