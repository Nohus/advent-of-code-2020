package day23_2

import solveInputs

fun main() {
    solveInputs { input ->
        val cups = input.chunked(1).map { it.toInt() } + (10..1_000_000)
        val next = (cups + cups.first()).zipWithNext().map { it.first to it.second }.toMap().toMutableMap()
        var currentCup = cups.first()

        repeat(10_000_000) {
            val firstPick = next.getValue(currentCup)
            val secondPick = next.getValue(firstPick)
            val thirdPick = next.getValue(secondPick)
            next[currentCup] = next.getValue(thirdPick)

            var destinationCup = currentCup - 1
            while (destinationCup in listOf(firstPick, secondPick, thirdPick) || destinationCup < 1) {
                destinationCup--
                if (destinationCup < 1) destinationCup = 1_000_000
            }

            val afterDestination = next.getValue(destinationCup)
            next[destinationCup] = firstPick
            next[thirdPick] = afterDestination

            currentCup = next.getValue(currentCup)
        }

        next.getValue(1).let { it.toLong() * next.getValue(it) }
    }
}
