package day22_1

import solveInputs

data class Result(val winner: Int, val deck: List<Int>)

fun main() {
    solveInputs { input ->
        val (deckA, deckB) = input.split("\n\n").map { playerInput ->
            playerInput.lines().drop(1).map { it.toInt() }.toMutableList()
        }
        val result = combat(deckA.toMutableList(), deckB.toMutableList())
        result.deck.reversed().mapIndexed { index, i -> i * (index + 1) }.sum()
    }
}

private fun combat(deckA: MutableList<Int>, deckB: MutableList<Int>): Result {
    while (deckA.isNotEmpty() && deckB.isNotEmpty()) {
        val a = deckA.removeFirst()
        val b = deckB.removeFirst()
        if (a > b) {
            deckA += a
            deckA += b
        } else {
            deckB += b
            deckB += a
        }
    }
    return if (deckA.isNotEmpty()) Result(0, deckA)
    else Result(1, deckB)
}
