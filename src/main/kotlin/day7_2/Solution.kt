package day7_2

import solveInputs

fun main() {
    solveInputs { input ->
        val tree = input.lines().map { line ->
            val outer = line.substringBefore(" bags")
            val inner = line.substringAfter("contain ").substringBeforeLast(".").split(", ").map {
                    val count = it.substringBefore(" ").toIntOrNull() ?: 0
                    val bag = it.substringAfter(" ").substringBefore(" bag")
                    count to bag
                }
            outer to inner
        }.toMap()
        var total = 0
        val toCheck = mutableListOf(1 to "shiny gold")
        while (toCheck.isNotEmpty()) {
            val outer = toCheck.removeFirst()
            val inner = tree[outer.second] ?: emptyList()
            total += inner.map { it.first * outer.first }.sum()
            toCheck += inner.map { (it.first * outer.first) to it.second }
        }
        total
    }
}
