package day12_2

import solveInputs
import utils.Direction.*
import utils.Point

fun main() {
    solveInputs { input ->
        var ship = Point.ORIGIN
        var waypoint = Point(10, -1)
        input.lines().forEach { line ->
            val value = line.substring(1).toInt()
            when (line.first()) {
                'N' -> waypoint = waypoint.move(NORTH, value)
                'S' -> waypoint = waypoint.move(SOUTH, value)
                'W' -> waypoint = waypoint.move(WEST, value)
                'E' -> waypoint = waypoint.move(EAST, value)
                'L' -> repeat(value / 90) { waypoint = Point(waypoint.y, -waypoint.x) }
                'R' -> repeat(value / 90) { waypoint = Point(-waypoint.y, waypoint.x) }
                'F' -> repeat(value) { ship += waypoint }
            }
        }
        Point.ORIGIN.distanceTo(ship)
    }
}
