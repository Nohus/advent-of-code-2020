package day4_1

import solveInputs

fun main() {
    solveInputs { input ->
        val passports = input.split("\n\n")
        val requiredFields = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
        passports.count { passport ->
            val fields = passport.split(" ", "\n").map { it.split(":")[0] }
            requiredFields.all { required -> fields.any { it == required } }
        }
    }
}
