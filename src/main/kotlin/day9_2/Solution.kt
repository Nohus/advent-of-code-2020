package day9_2

import solveInputs

fun main() {
    solveInputs { input ->
        val numbers = input.lines().map { it.toLong() }
        val invalid = numbers.windowed(25 + 1).first { window ->
            val tail = window.dropLast(1)
            tail.none { window.last() - it in tail }
        }.last()
        numbers.indices.asSequence().mapNotNull { i ->
            var sum = 0L
            val elements = numbers.drop(i).takeWhile { sum += it; sum < invalid }
            if (sum == invalid) elements.minOrNull()!! + elements.maxOrNull()!!
            else null
        }.first()
    }
}
