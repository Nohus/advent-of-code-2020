package day20_2

import solveInputs
import utils.Direction
import utils.Direction.EAST
import utils.Direction.NORTH
import utils.Direction.SOUTH
import utils.Direction.WEST
import utils.Point
import kotlin.math.sqrt

typealias Tile = Map<Point, Boolean>
typealias Edge = List<Boolean>

fun main() {
    solveInputs { input ->
        val tiles: MutableMap<Int, Tile> = input.split("\n\n").map { tileData ->
            val id = tileData.lines().first().substringAfter(" ").substringBefore(":").toInt()
            val tile = mutableMapOf<Point, Boolean>()
            tileData.lines().drop(1).forEachIndexed { y, line ->
                line.forEachIndexed { x, char ->
                    tile[Point(x, y)] = char == '#'
                }
            }
            id to tile
        }.toMap().toMutableMap()
        val edges = tiles.map { (id, tile) ->
            id to getEdges(tile)
        }.toMap()
        val matches = tiles.keys.map {
            it to getMatchingTiles(it, edges)
        }.toMap()
        val corner = matches.filter { it.value.size == 2 }.map { it.key }.first()

        // Create the tile grid
        val gridSideSize = sqrt(tiles.size.toDouble()).toInt()
        val grid = mutableMapOf<Point, Int?>()
        for (y in 0 until gridSideSize) {
            for (x in 0 until gridSideSize) {
                grid[Point(x, y)] = null
            }
        }

        // Fill grid with matching tiles
        grid[Point(0, 0)] = corner
        while (grid.values.any { it == null }) {
            val firstEmptyPosition = grid.entries.first { it.value == null }.key
            val adjacentTiles = firstEmptyPosition.getAdjacentSides().mapNotNull { grid[it] }
            val adjacentSlots = firstEmptyPosition.getAdjacentSides().filter { it in grid.keys }.size
            val matchingTile = matches.filter { (id, tiles) ->
                id !in grid.values && adjacentTiles.all { it in tiles } && tiles.size == adjacentSlots
            }.map { it.key }.first()
            grid[firstEmptyPosition] = matchingTile
        }

        // Rotate/flip each tile to match adjacent tiles
        grid.keys.forEach { point ->
            val tile = tiles[grid[point]]!!
            val matchingOrientationsPerDirection = Direction.values().map { direction ->
                tiles[grid[point.move(direction)]]?.let {
                    getMatchingEdges(tile, it).map { edge ->
                        tile.orient(edge, direction)
                    }
                }
            }
            val validOrientation = matchingOrientationsPerDirection.first { it != null }!!.first { orientation ->
                matchingOrientationsPerDirection.all { it == null || orientation in it }
            }
            tiles[grid[point]!!] = validOrientation
        }

        // Join tiles into a single image
        val image = mutableMapOf<Point, Boolean>()
        grid.keys.forEach { point ->
            val tile = tiles[grid[point]!!]!!.stripBorders()
            val height = tile.maxOf { it.key.y } + 1
            val width = tile.maxOf { it.key.x } + 1
            tile.forEach { (tilePoint, pixel) ->
                val shiftedPoint = Point(point.x * width + tilePoint.x, point.y * height + tilePoint.y)
                image[shiftedPoint] = pixel
            }
        }

        // Create 8 rotated/flipped versions of the image
        val images = List(4) {
            var rotatedImage = image.toMap()
            repeat(it) { rotatedImage = rotatedImage.clockwiseTurn() }
            rotatedImage
        }.flatMap { listOf(it, it.horizontalFlip()) }

        // Assemble the sea monster pattern
        val seaMonsterText = """
                              # 
            #    ##    ##    ###
             #  #  #  #  #  #   
        """.trimIndent()
        val seaMonster = mutableSetOf<Point>()
        seaMonsterText.lines().forEachIndexed { y, line ->
            line.forEachIndexed { x, char ->
                if (char == '#') seaMonster += Point(x, y)
            }
        }

        // Search for sea monsters
        images.asSequence().mapIndexedNotNull { index, rotatedImage ->
            val seaMonsterCount = rotatedImage.keys.count { point ->
                seaMonster.all { offset -> rotatedImage[point + offset] == true }
            }
            if (seaMonsterCount > 0) rotatedImage.values.count { it } - seaMonster.size * seaMonsterCount
            else null
        }.first()
    }
}

private fun getMatchingTiles(tile: Int, edges: Map<Int, List<Edge>>): List<Int> {
    return edges[tile]!!.mapNotNull { tileEdge ->
        val matching = edges.entries.filterNot { it.key == tile }.firstOrNull { (_, otherEdges) ->
            otherEdges.any { otherEdge -> otherEdge == tileEdge }
        }
        matching?.key
    }.distinct()
}

private fun getEdges(tile: Tile): List<List<Boolean>> {
    val maxY = tile.maxOf { it.key.y }
    val maxX = tile.maxOf { it.key.x }
    val top = (0..maxX).map { tile[Point(it, 0)]!! }
    val bottom = (0..maxX).map { tile[Point(it, maxY)]!! }
    val left = (0..maxY).map { tile[Point(0, it)]!! }
    val right = (0..maxY).map { tile[Point(maxX, it)]!! }
    return listOf(top, bottom, left, right, top.reversed(), bottom.reversed(), left.reversed(), right.reversed())
}

private fun getMatchingEdges(tileA: Tile, tileB: Tile): List<Edge> {
    val edgesA = getEdges(tileA)
    val edgesB = getEdges(tileB)
    return edgesA.filter { it in edgesB }
}

private fun Tile.orient(edge: Edge, direction: Direction): Tile {
    return when (direction) {
        NORTH -> orientForTopEdge(edge)
        EAST -> orientForRightEdge(edge)
        SOUTH -> orientForBottomEdge(edge)
        WEST -> orientForLeftEdge(edge)
    }
}

private fun Tile.orientForRightEdge(edge: Edge): Tile {
    return when (getEdges(this).indexOf(edge)) {
        0 -> this.clockwiseTurn()
        1 -> this.verticalFlip().clockwiseTurn()
        2 -> this.horizontalFlip()
        3 -> this
        4 -> this.horizontalFlip().clockwiseTurn()
        5 -> this.horizontalFlip().verticalFlip().clockwiseTurn()
        6 -> this.verticalFlip().horizontalFlip()
        7 -> this.verticalFlip()
        else -> throw IllegalArgumentException()
    }
}

private fun Tile.orientForBottomEdge(edge: Edge): Tile {
    return orientForRightEdge(edge).clockwiseTurn().horizontalFlip()
}

private fun Tile.orientForLeftEdge(edge: Edge): Tile {
    return orientForRightEdge(edge).horizontalFlip()
}

private fun Tile.orientForTopEdge(edge: Edge): Tile {
    return orientForRightEdge(edge).clockwiseTurn().clockwiseTurn().clockwiseTurn()
}

private fun Tile.clockwiseTurn(): Tile {
    val maxX = maxOf { it.key.x }
    return transpose { Point(maxX - it.y, it.x) }
}

private fun Tile.horizontalFlip(): Tile {
    val maxX = maxOf { it.key.x }
    return transpose { Point(maxX - it.x, it.y) }
}

private fun Tile.verticalFlip(): Tile {
    val maxY = maxOf { it.key.y }
    return transpose { Point(it.x, maxY - it.y) }
}

private fun Tile.transpose(pointTransform: (Point) -> Point): Tile {
    val newTile = mutableMapOf<Point, Boolean>()
    entries.forEach { (point, pixel) ->
        val newPoint = pointTransform(point)
        newTile[newPoint] = pixel
    }
    return newTile
}

private fun Tile.stripBorders(): Tile {
    val maxY = maxOf { it.key.y }
    val maxX = maxOf { it.key.x }
    return filter { (point, _) -> point.x in 1 until maxX && point.y in 1 until maxY }
        .map { (point, pixel) -> Point(point.x - 1, point.y - 1) to pixel }
        .toMap()
}

// Debugging

private fun printEdge(edge: Edge) {
    print("=== Edge === ")
    for (x in edge) {
        print(if (x) "#" else ".")
    }
    println()
}

private fun printTile(tile: Tile) {
    println("=== Tile ===")
    val maxY = tile.maxOf { it.key.y }
    val maxX = tile.maxOf { it.key.x }
    for (y in 0..maxY) {
        for (x in 0..maxX) {
            val pixel = tile[Point(x, y)]!!
            print(if (pixel) "#" else ".")
        }
        println()
    }
}
