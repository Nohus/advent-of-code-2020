package day18_1

import day18_1.Expression.Addition
import day18_1.Expression.Multiplication
import day18_1.Expression.Num
import solveInputs

sealed class Expression {
    abstract fun solve(): Long
    data class Num(val value: Long) : Expression() {
        override fun solve() = value
    }
    data class Addition(val a: Expression, val b: Expression) : Expression() {
        override fun solve() = a.solve() + b.solve()
    }
    data class Multiplication(val a: Expression, val b: Expression) : Expression() {
        override fun solve() = a.solve() * b.solve()
    }
}

private fun parse(text: String): Expression {
    text.trim().toLongOrNull()?.let { return Num(it) }
    return if (text.endsWith(")")) {
        var nesting = 0
        var startIndex = 0
        for (i in text.lastIndex downTo 0) {
            when (text[i]) {
                '(' -> nesting--
                ')' -> nesting++
            }
            if (nesting == 0) {
                startIndex = i
                break
            }
        }
        val right = parse(text.substring(startIndex).removeSurrounding("(", ")"))
        val remaining = text.substring(0, startIndex)
        if (remaining.isNotEmpty()) parseRemaining(remaining, right) else right
    } else {
        val right = Num(text.substringAfterLast(" ").toLong())
        val remaining = text.substringBeforeLast(" ")
        parseRemaining(remaining, right)
    }
}

private fun parseRemaining(remaining: String, right: Expression): Expression {
    val operation = remaining.trim().substringAfterLast(" ")
    val left = parse(remaining.trim().substringBeforeLast(" "))
    return when (operation) {
        "+" -> Addition(left, right)
        "*" -> Multiplication(left, right)
        else -> throw IllegalArgumentException()
    }
}

fun main() {
    solveInputs { input ->
        input.lines().map { parse(it).solve() }.sum()
    }
}
