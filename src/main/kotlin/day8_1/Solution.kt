package day8_1

import solveInputs

data class Instruction(
    val name: String,
    val argument: Int
)

fun main() {
    solveInputs { input ->
        val code = input.lines().map {
            val (name, argument) = it.split(" ")
            Instruction(name, argument.toInt())
        }
        run(code)
    }
}

fun run(code: List<Instruction>): Int {
    var accumulator = 0
    var pc = 0
    val visited = mutableListOf<Int>()
    while (true) {
        val instruction = code[pc]
        when (instruction.name) {
            "acc" -> accumulator += instruction.argument
            "jmp" -> pc += instruction.argument - 1
        }
        visited += pc
        pc++
        if (pc in visited) return accumulator
    }
}
