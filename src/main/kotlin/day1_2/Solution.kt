package day1_2

import solveInputs

fun main() {
    solveInputs { input ->
        val numbers = input.lines().map(String::toInt)
        for (a in numbers) {
            for (b in numbers) {
                for (c in numbers) {
                    if (a + b + c == 2020) return@solveInputs a * b * c
                }
            }
        }
    }
}
