package day11_1

import day11_1.Tile.*
import solveInputs
import utils.Point

enum class Tile {
    FLOOR, EMPTY, OCCUPIED
}

fun main() {
    solveInputs { input ->
        val inputMap = mutableMapOf<Point, Tile>()
        input.lines().forEachIndexed { y, line ->
            line.forEachIndexed { x, char ->
                inputMap[Point(x, y)] = if (char == 'L') EMPTY else FLOOR
            }
        }
        var map: Map<Point, Tile> = inputMap
        while (true) {
            val newMap = applyRound(map)
            if (newMap == map) break
            map = newMap
        }
        map.count { (_, tile) -> tile == OCCUPIED }
    }
}

private fun applyRound(map: Map<Point, Tile>): Map<Point, Tile> {
    val new = mutableMapOf<Point, Tile>()
    map.forEach { (point, tile) ->
        when {
            tile == EMPTY && point.getAdjacent().none { map[it] == OCCUPIED } -> new[point] = OCCUPIED
            tile == OCCUPIED && point.getAdjacent().count { map[it] == OCCUPIED } >= 4 -> new[point] = EMPTY
            else -> new[point] = map[point]!!
        }
    }
    return new
}
