package day7_1

import solveInputs

fun main() {
    solveInputs { input ->
        val tree = input.lines().map { line ->
            val outer = line.substringBefore(" bags")
            val inner = line.substringAfter("contain ").substringBeforeLast(".").split(", ").map {
                it.substringAfter(" ").substringBefore(" bag")
            }
            outer to inner
        }.toMap()
        val possible = mutableListOf<String>()
        val toCheck = mutableListOf("shiny gold")
        while (toCheck.isNotEmpty()) {
            val outer = toCheck.flatMap { bag -> getOuter(tree, bag).filter { it !in possible } }.toSet()
            toCheck.clear()
            toCheck += outer
            possible += outer
        }
        possible.size
    }
}

fun getOuter(tree: Map<String, List<String>>, inner: String): List<String> {
    return tree.filter { (_, thisInner) -> thisInner.any { it == inner } }.map { it.key }
}
