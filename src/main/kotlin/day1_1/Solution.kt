package day1_1

import solveInputs

fun main() {
    solveInputs { input ->
        val numbers = input.lines().map(String::toInt)
        for (a in numbers) {
            for (b in numbers) {
                if (a + b == 2020) return@solveInputs a * b
            }
        }
    }
}
