package day24_2

import day24_2.Direction.*
import solveInputs
import utils.Point

enum class Direction {
    EAST, SOUTH_EAST, SOUTH_WEST, WEST, NORTH_WEST, NORTH_EAST
}

fun main() {
    solveInputs { input ->
        val paths = input.lines().map {
            it.replace("se", "1").replace("sw", "2")
                .replace("nw", "3").replace("ne", "4")
                .map {
                    when (it) {
                        'e' -> EAST
                        '1' -> SOUTH_EAST
                        '2' -> SOUTH_WEST
                        'w' -> WEST
                        '3' -> NORTH_WEST
                        '4' -> NORTH_EAST
                        else -> throw IllegalArgumentException()
                    }
                }
        }
        val black = mutableSetOf<Point>()
        paths.forEach { path ->
            var position = Point(0, 0)
            path.forEach { position = position.move(it) }
            if (position in black) black -= position
            else black += position
        }

        repeat(100) {
            val white = (black.flatMap { it.getAdjacentHex() }.toSet() - black)
            val flipToWhite = black.filter {
                it.getAdjacentHex().count { it in black }.let { it == 0 || it > 2 }
            }
            val flipToBlack = white.filter {
                it.getAdjacentHex().count { it in black } == 2
            }
            black -= flipToWhite
            black += flipToBlack
        }

        black.size
    }
}

private fun Point.move(direction: Direction): Point {
    val even = y % 2 == 0
    return when (direction) {
        EAST -> Point(x + 1, y)
        SOUTH_EAST -> {
            if (even) Point(x, y + 1)
            else Point(x + 1, y + 1)
        }
        SOUTH_WEST -> {
            if (even) Point(x - 1, y + 1)
            else Point(x, y + 1)
        }
        WEST -> Point(x - 1, y)
        NORTH_WEST -> {
            if (even) Point(x - 1, y - 1)
            else Point(x, y - 1)
        }
        NORTH_EAST -> {
            if (even) Point(x, y - 1)
            else Point(x + 1, y - 1)
        }
    }
}

private fun Point.getAdjacentHex(): List<Point> {
    return Direction.values().map {
        move(it)
    }
}
