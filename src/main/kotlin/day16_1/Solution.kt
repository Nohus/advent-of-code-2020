package day16_1

import solveInputs

fun main() {
    solveInputs { input ->
        val rules = input.substringBefore("\n\n").lines().map {
            it.substringAfter(": ").split(" or ").map { range ->
                range.substringBefore("-").toInt()..range.substringAfter("-").toInt()
            }
        }.flatten()
        input.substringAfter("nearby tickets:\n").lines().map {
            it.split(",").map { it.toInt() }
        }.map {
            it.filterNot { field -> rules.any { rule -> field in rule } }
        }.flatten().sum()
    }
}
