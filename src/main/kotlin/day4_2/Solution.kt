package day4_2

import solveInputs

fun main() {
    solveInputs { input ->
        val passports = input.split("\n\n")
        val requiredFields = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
        passports.count { passport ->
            val fields = passport.split(" ", "\n").map { it.split(":").let { it[0] to it[1] } }
            requiredFields.all { required ->
                fields.any {
                    val value = it.second
                    it.first == required && when (it.first) {
                        "byr" -> value.toInt() in 1920..2002
                        "iyr" -> value.toInt() in 2010..2020
                        "eyr" -> value.toInt() in 2020..2030
                        "hgt" -> when {
                            value.endsWith("cm") -> value.substringBefore("cm").toInt() in 150..193
                            value.endsWith("in") -> value.substringBefore("in").toInt() in 59..76
                            else -> false
                        }
                        "hcl" -> value.matches("""#[0-9a-f]{6}""".toRegex())
                        "ecl" -> value in listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
                        "pid" -> value.matches("""[0-9]{9}""".toRegex())
                        else -> true
                    }
                }
            }
        }
    }
}
